import React from "react";
import axios from "axios";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      body: "",
      author: ""
    };

    this.handleChangeField = this.handleChangeField.bind(this);
  }

  handleChangeField(key, event) {
    console.log(key, event);
    // this.setState({
    //   [key]: event.target.value
    // });
  }
  //   handleSubmit() {
  //     const { title, body, author } = this.state;

  //     return axios.post(" ", {
  //       title,
  //       body,
  //       author
  //     });
  //   }

  render() {
    const { title, body, author } = this.state;

    return (
      <div className="col-12 col-lg-6 offset-lg-3">
        <input
          onChange={ev => this.handleChangeField("title", ev)}
          value={title}
          className="form-control my-3"
          placeholder="Article Title"
        />

        <CKEditor
          editor={ClassicEditor}
          onChange={(event, editor) => {
            const data = editor.getData();
            console.log({ event, editor, data });
          }}
          config={{
            ckfinder: {
              uploadUrl: 'uploadUrlHere'
            }
          }}
          className="form-control my-3"
          placeholder="Article Body"
          value={body}
          contentEditable="true"
        />
        {/* <textarea
          onChange={ev => this.handleChangeField("body", ev)}
          className="form-control my-3"
          placeholder="Article Body"
          value={body}
        ></textarea> */}
        <input
          onChange={ev => this.handleChangeField("author", ev)}
          value={author}
          className="form-control my-3"
          placeholder="Article Author"
        />
        {/* <button
          onClick={this.handleSubmit()}
          className="btn btn-primary float-right"
        >
          Submit
        </button> */}
      </div>
    );
  }
}

export default Form;
